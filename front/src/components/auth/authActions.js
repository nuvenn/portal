import { toastr } from "react-redux-toastr";
import axios from "axios";
import HttpService from "../../services/HttpService";
import consts from "../../consts";

export function login(values, callback) {
  return submit(values, callback);
}

export function signup(values) {
  return submit(values);
}

function submit(values, callback) {
  return async dispatch => {
    let instance = {
      method: "POST",
      url: `${consts.BASE_URL}/auth`,
      data: values,
      headers: { "Content-type": "application/x-www-form-urlencoded" },
      withCredentials: true
    };
    try {
      return await axios(instance).then(resp => {
        callback(resp);
        dispatch([{ type: "USER_FETCHED", payload: resp.data }]);
      });
    } catch (error) {
      toastr.error("Erro", error.response.data);
    }
  };
}

export function refresh(user) {
  return dispatch => {
    dispatch([{ type: "SET_USER", payload: user }]);
  };
}

export function logout(callback) {
  return dispatch => {
    HttpService.request("GET", `${consts.BASE_URL}/logout`).then(resp => {
      callback(resp);
      dispatch([{ type: "LOGOUT", payload: {} }]);
    });
  };
}
