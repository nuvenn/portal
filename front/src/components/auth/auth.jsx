import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import Input from "../common/login/input";
import "../../sass/App.scss";

import { login } from "./authActions";

export default function Auth() {
  const [user, setUser] = useState({ username: "", password: "" });
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();

  function onSubmit(e) {
    e.preventDefault();
    const userCredentials = `username=${user.username}&password=${user.password}`;
    dispatch(login(userCredentials, loginCallBack));
  }

  function loginCallBack(response) {
    localStorage.setItem("_u", JSON.stringify(response.data));
  }

  if (
    JSON.parse(localStorage.getItem("_u")) &&
    auth.user.id === JSON.parse(localStorage.getItem("_u")).id
  )
    return <Redirect to="/situacaofuncional/situacaofuncional/" />;
  return (
    <form onSubmit={e => onSubmit(e)}>
      <div className="login">
        <div className="login__panel-container">
          <div className="login__form">
            <div className="login__image"></div>
            <Input
              type="input"
              name="username"
              placeholder="Usuário"
              value={user.username}
              onChange={e => setUser({ ...user, username: e.target.value })}
            />
            <Input
              type="password"
              name="password"
              placeholder="Senha"
              value={user.password}
              onChange={e => setUser({ ...user, password: e.target.value })}
            />
            <button className="button button--primary button--block">
              Entrar
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}
