const INITIAL_STATE = {
  user: {},
  logged: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_USER":
      return { ...state, user: action.payload, logged: true };
    case "USER_FETCHED":
      return { ...state, user: action.payload, logged: true };
    case "LOGOUT":
      return { ...state, user: action.payload, logged: false };
    default:
      return state;
  }
};
