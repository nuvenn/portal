import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

import { formatDate } from "../../services/DateService";

export default function MediaControlCard(props) {
  const { retratoFuncional } = props;
  return (
    <Card className="card">
      <div className="card__details">
        <CardContent className="card__content">
          <Typography component="h6" variant="h6" className="card__title">
            RETRATO FUNCIONAL
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Vínculo: {retratoFuncional.ident_vinc}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Início do exercício:{" "}
            {formatDate(retratoFuncional.dtexerc, "DD-MM-YYYY")}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Detalhes do vínculo: {retratoFuncional.detalhesVinculo}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Situação: {retratoFuncional.situacao}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Cargo/Função: {retratoFuncional.cargo_descr}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Referência salarial: {retratoFuncional.referencia}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Lotação: {retratoFuncional.setor_descr}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Órgão/Empresa: {retratoFuncional.nomeSubempresa}
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
}
