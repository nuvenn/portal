import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

export default function MediaControlCard(props) {
  const { situacaoFuncional } = props;
  return (
    <Card className="card">
      <div className="card__details">
        <CardContent className="card__content">
          <Typography component="h6" variant="h6" className="card__title">
            DADOS PESSOAIS
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Identificação funcional: {situacaoFuncional.numfunc}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            CPF: {situacaoFuncional.cpf}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Documento: {situacaoFuncional.tiporg}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Estado civil: {situacaoFuncional.estcivil}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Escolaridade: {situacaoFuncional.descricaoEscolaridade}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Telefone: {situacaoFuncional.telefone_fixo}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Ramal: {situacaoFuncional.ramal}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            Celular: {situacaoFuncional.celular}
          </Typography>
          <Typography
            variant="subtitle2"
            color="textSecondary"
            className="card__paragraph"
          >
            E-mail: {situacaoFuncional.e_mail}
          </Typography>
        </CardContent>
      </div>
      <CardMedia className="card__cover" title="Foto de identificação">
        <img src={situacaoFuncional.fotoString} alt="Foto do funcionário" />
      </CardMedia>
    </Card>
  );
}
