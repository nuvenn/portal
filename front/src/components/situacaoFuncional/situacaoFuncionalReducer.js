const INITIAL_STATE = {
  situacao: [],
  retrato: [],
  dependentesBenef: [],
  dependentesPA: [],
  feriasSituacao: [],
  feriasVencimentos: [],
  afastamentos: [],
  solicitacoes: [],
  pagamentos: []
};

export default (state = INITIAL_STATE, action = { payload: {} }) => {
  switch (action.type) {
    case "SITUACAO_FUNCIONAL_FETCHED":
      return {
        ...state,
        situacao: action.payload.data ? action.payload.data[0] : []
      };
    case "RETRATO_FUNCIONAL_FETCHED":
      return {
        ...state,
        retrato: action.payload.data ? action.payload.data[0] : []
      };
    case "DEPENDENTES_BENEF_FETCHED":
      return {
        ...state,
        dependentesBenef: action.payload.data ? action.payload.data[0] : []
      };
    case "DEPENDENTES_PA_FETCHED":
      return {
        ...state,
        dependentesPA: action.payload.data ? action.payload.data[0] : []
      };
    case "FERIAS_SITUACAO_FETCHED":
      return {
        ...state,
        feriasSituacao: action.payload.data ? action.payload.data[0] : []
      };
    case "FERIAS_VENCIMENTOS_FETCHED":
      return {
        ...state,
        feriasVencimentos: action.payload.data ? action.payload.data[0] : []
      };
    case "AFASTAMENTOS_FETCHED":
      return {
        ...state,
        afastamentos: action.payload.data ? action.payload.data[0] : []
      };
    case "SOLICITACOES_FETCHED":
      return {
        ...state,
        solicitacoes: action.payload.data ? action.payload.data[0] : []
      };
    case "PAGAMENTOS_FETCHED":
      return {
        ...state,
        pagamentos: action.payload.data ? action.payload.data[0] : []
      };
    default:
      return state;
  }
};
