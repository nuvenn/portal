import React, { useEffect, useContext } from "react";
import { __RouterContext } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import If from "../common/operator/if";
import PageHeader from "../common/template/pageHeader";
import BlocoDadosPessoais from "./blocoDadosPessoais";
import BlocoRetratoFuncional from "./blocoRetratoFuncional";
import Chart from "../common/components/pieChart";
import BoxContainer from "../common/components/box";
import { getVinculos } from "../vinculos/vinculosActions";
import {
  getSituacaoFuncional,
  getRetratoFuncional,
  getAfastamentos,
  getDependentesComPA,
  getDependentesPorBenef,
  getFeriasSituacao,
  getFeriasVencimentos,
  getPagamentos,
  getSolicitacoes
} from "./situacaoFuncionalActions";

export default function SituacaoFuncional() {
  const router = useContext(__RouterContext);
  const auth = useSelector(state => state.auth);
  const vinculos = useSelector(state => state.vinculos);
  const situacaoFuncional = useSelector(state => state.situacaoFuncional);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getSituacaoFuncional(auth.user));
    dispatch(getRetratoFuncional(auth.user));
  }, [dispatch, auth]);

  useEffect(() => {
    if (auth.logged === true) {
      dispatch(getVinculos(auth.user));
    }
  }, [dispatch, auth]);

  useEffect(() => {
    if (vinculos.ativo && vinculos.ativo.numfunc) {
      dispatch(getAfastamentos(vinculos.ativo));
      dispatch(getDependentesComPA(vinculos.ativo));
      dispatch(getDependentesPorBenef(vinculos.ativo));
      dispatch(getFeriasSituacao(vinculos.ativo));
      dispatch(getFeriasVencimentos(vinculos.ativo));
      dispatch(getPagamentos(vinculos.ativo));
      dispatch(getSolicitacoes(vinculos.ativo));
    }
  }, [dispatch, vinculos.ativo]);

  function redirect(path) {
    router.history.push(path);
  }

  return (
    <>
      <PageHeader title="Situação Funcional" />
      <BoxContainer justify="center" marginTop={2}>
        <Grid item xs={12} md={5} lg={5} xl={4}>
          <If test={situacaoFuncional}>
            <Chart
              data={[
                situacaoFuncional.feriasSituacao ||
                situacaoFuncional.feriasVencimentos
                  ? {
                      x: "Férias",
                      y: 20,
                      status: `Férias: ${situacaoFuncional.feriasSituacao.saldoDiasDescricao}`
                    }
                  : { x: " ", y: 0 },
                situacaoFuncional.afastamentos
                  ? { x: "Afastamentos", y: 20 }
                  : { x: " ", y: 0 },
                situacaoFuncional.solicitacoes
                  ? {
                      x: "Solicitações",
                      y: 20,
                      status: `Solicitações: ${situacaoFuncional.solicitacoes.totalSolicitacoesAbertasDescr}`
                    }
                  : { x: " ", y: 0 },
                situacaoFuncional.pagamentos
                  ? { x: "Pagamentos", y: 20 }
                  : { x: " ", y: 0 },
                situacaoFuncional.dependentesPA ||
                situacaoFuncional.dependentesBenef
                  ? { x: "Dependentes", y: 20 }
                  : { x: " ", y: 0 }
              ]}
              paths={[
                "/situacaofuncional/frequencia",
                "/",
                "/solicitacoes/solicitacoes",
                "/",
                "/"
              ]}
              redirect={redirect}
            />
          </If>
        </Grid>
        <Grid item xs={12} md={5} lg={5} xl={4}>
          <BlocoDadosPessoais situacaoFuncional={situacaoFuncional.situacao} />
        </Grid>
      </BoxContainer>
      <BoxContainer justify="center" marginBottom={2}>
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <BlocoRetratoFuncional retratoFuncional={situacaoFuncional.retrato} />
        </Grid>
      </BoxContainer>
    </>
  );
}
