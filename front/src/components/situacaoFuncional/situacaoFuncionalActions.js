import HttpService from "../../services/HttpService";
import consts from "../../consts";

export function getSituacaoFuncional(funcionario) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/Funcionarios/listByNumfunc/${funcionario.numfunc}`
  );
  return {
    type: "SITUACAO_FUNCIONAL_FETCHED",
    payload: request
  };
}

export function getRetratoFuncional(funcionario) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/RetratoFuncional/listPorNumfunc/${funcionario.numfunc}`
  );
  return {
    type: "RETRATO_FUNCIONAL_FETCHED",
    payload: request
  };
}

export function getDependentesPorBenef(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeDependentes/listTotalDeDependentesPorBeneficios/${vinculo.numfunc}`
  );
  return {
    type: "DEPENDENTES_BENEF_FETCHED",
    payload: request
  };
}

export function getDependentesComPA(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeDependentes/listTotalDeDependentesComPA/${vinculo.numfunc}`
  );
  return {
    type: "DEPENDENTES_PA_FETCHED",
    payload: request
  };
}

export function getFeriasSituacao(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeFerias/listByNumfuncENumvincESituacao/${vinculo.numfunc}/${vinculo.numvinc}/ATIVO`
  );
  return {
    type: "FERIAS_SITUACAO_FETCHED",
    payload: request
  };
}

export function getFeriasVencimentos(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeFerias/listPrevisaoVencimentoDasProximasFerias/${vinculo.numfunc}/${vinculo.numvinc}`
  );
  return {
    type: "FERIAS_VENCIMENTOS_FETCHED",
    payload: request
  };
}

export function getAfastamentos(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeAfastamentos/listByNumfuncENumvinc/${vinculo.numfunc}/${vinculo.numvinc}`
  );
  return {
    type: "AFASTAMENTOS_FETCHED",
    payload: request
  };
}

export function getSolicitacoes(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDeSolicitacoes/listByNumfuncENumvinc/${vinculo.numfunc}/${vinculo.numvinc}`
  );
  return {
    type: "SOLICITACOES_FETCHED",
    payload: request
  };
}

export function getPagamentos(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/ResumoDePagamentos/listByNumfuncENumvinc/${vinculo.numfunc}/${vinculo.numvinc}`
  );
  return {
    type: "PAGAMENTOS_FETCHED",
    payload: request
  };
}
