import React, { useContext } from "react";
import { __RouterContext } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const useStyles = makeStyles(theme => ({
  actions: {
    width: "100%",
    backgroundColor: theme.palette.primary.greyLight2,
    padding: "1rem 8rem",
    display: "flex",
    justifyContent: "space-between"
  },
  title: {
    color: theme.palette.primary.greyDark2
  },
  iconbutton: {
    width: "25px",
    height: "25px"
  }
}));

export default function PageHeader(props) {
  const router = useContext(__RouterContext);
  const classes = useStyles();

  function goBack() {
    router.history.goBack();
  }

  return (
    <>
      <div className={classes.actions}>
        <Typography variant="h6" className={classes.title}>
          {props.title.toUpperCase()}
        </Typography>
        <IconButton
          color="primary"
          onClick={goBack}
          aria-label="voltar"
          size="small"
        >
          <ArrowBackIcon fontSize="inherit" className={classes.iconbutton} />
        </IconButton>
      </div>
    </>
  );
}
