import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Collapse from "@material-ui/core/Collapse";

import theme from "../../../theme";

export default props => {
  const [open0, setOpen0] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [open3, setOpen3] = useState(false);
  const [open4, setOpen4] = useState(false);
  const [open5, setOpen5] = useState(false);
  const [open6, setOpen6] = useState(false);

  return (
    <aside className="menu">
      <ul>
        <li>
          <button onClick={() => setOpen0(!open0)}>Vida Funcional</button>
        </li>
        <Collapse in={open0} timeout={500}>
          <ul>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/situacaofuncional/"
              >
                Situação funcional
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/cadastro/"
              >
                Cadastro
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/dependentes/"
              >
                Dependentes e benefícios
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/retrato/"
              >
                Retrato funcional
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/frequencia/"
              >
                Frequência
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodevinculos/"
              >
                Histórico de vínculos
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodecargos/"
              >
                Histórico de cargos e funções
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodevantagens/"
              >
                Histórico de vantagens
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodefrequencias/"
              >
                Histórico de frequências
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodecessoes/"
              >
                Histórico de cessões
              </NavLink>
            </li>
            <li>
              <NavLink
                activeStyle={{ backgroundColor: theme.palette.primary.main }}
                to="/situacaofuncional/historicodeatividades/"
              >
                Histórico de atividades
              </NavLink>
            </li>
          </ul>
        </Collapse>
        <li>
          <button onClick={() => setOpen1(!open1)}>Pagamentos</button>
        </li>
        <li>
          <button onClick={() => setOpen2(!open2)}>Tempo de Serviço</button>
        </li>
        <li>
          <button onClick={() => setOpen3(!open3)}>Solicitações</button>
        </li>
        <li>
          <button onClick={() => setOpen4(!open4)}>Treinamento</button>
        </li>
        <li>
          <button onClick={() => setOpen5(!open5)}>Carreira</button>
        </li>
        <li>
          <button onClick={() => setOpen6(!open6)}>Perfil</button>
        </li>
      </ul>
    </aside>
  );
};
