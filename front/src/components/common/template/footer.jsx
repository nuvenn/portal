import React from "react";

export default props => (
  <div className="footer">
    <div className="footer__title">{props.title}</div>
  </div>
);
