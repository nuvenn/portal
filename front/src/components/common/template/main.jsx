import React from "react";

export default props => <div className="main">{props.children}</div>;
