import React from "react";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import { logout } from "../../auth/authActions";

const useStyles = makeStyles(theme => ({
  root: { marginRight: "20px" },
  menuItem: { paddingLeft: "34px", paddingRight: "34px" },
  menuIcon: {
    color: theme.palette.primary.colorWhite
  }
}));

export default () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function sair() {
    dispatch(logout(logoutCallback));
  }

  function logoutCallback(response) {
    localStorage.clear();
    window.location.reload();
  }

  return (
    <div className="header">
      <div className="login__image"></div>
      <div className={classes.root}>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
          className={classes.menuIcon}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose} className={classes.menuItem}>
            Ajuda
          </MenuItem>
          <MenuItem onClick={() => sair()} className={classes.menuItem}>
            Sair
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};
