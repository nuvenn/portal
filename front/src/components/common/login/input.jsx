import React from "react";

export default props => {
  return (
    <input
      {...props.input}
      className="login__input"
      placeholder={props.placeholder}
      type={props.type}
      onChange={props.onChange}
    />
  );
};
