import React from "react";
import { makeStyles } from "@material-ui/styles";
import { VictoryPie, VictoryTooltip } from "victory";

import GraphTooltip from "./graphTooltip";

const pallete = ["#BFD8D2", "#FEDCD2", "#DF744A", "#DCB239", "#FFEEAD"];
let legend = {};
for (let i = 0; i < pallete.length; i++) {
  legend[`legend_${i}`] = { backgroundColor: pallete[i] };
}

const useStyles = makeStyles(legend);

function Legend(props) {
  const classes = useStyles();
  return (
    <div className="chart__legend">
      <ul className="chart__list">
        <li className={`chart__item ${classes.legend_0}`}>Férias</li>
        <li className={`chart__item ${classes.legend_1}`}>Afastamentos</li>
        <li className={`chart__item ${classes.legend_2}`}>Solicitações</li>
        <li className={`chart__item ${classes.legend_3}`}>Pagamentos</li>
        <li className={`chart__item ${classes.legend_4}`}>Dependentes</li>
      </ul>
    </div>
  );
}

export default function PieChart(props) {
  return (
    <>
      <Legend />
      <VictoryPie
        height={300}
        cornerRadius={5}
        data={props.data}
        labels={() => ""}
        labelComponent={<VictoryTooltip flyoutComponent={<GraphTooltip />} />}
        events={[
          {
            target: "data",
            eventHandlers: {
              onClick: (evt, data) => {
                props.redirect(props.paths[data.index]);
              }
            }
          }
        ]}
        colorScale={pallete}
        style={{
          data: {
            cursor: "pointer"
          }
        }}
      />
    </>
  );
}
