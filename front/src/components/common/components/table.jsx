import React from "react";
import MaterialTable from "material-table";

export default function Tables(props) {
  return (
    <div className="table">
      <MaterialTable
        columns={props.columns}
        data={props.data}
        title={props.title}
        onRowClick={(event, rowData) => console.log(rowData)}
        editable={{
          onRowAdd: newData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                props.onAdd("add", newData);
                resolve();
              }, 1000);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                props.onEdit("update", newData);
                resolve();
              }, 1000);
            }),
          onRowDelete: oldData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                props.onDelete(oldData);
                resolve();
              }, 1000);
            })
        }}
        options={{
          actionsColumnIndex: -1,
          exportButton: true
        }}
        localization={{
          header: {
            actions: "Ações"
          },
          body: {
            emptyDataSourceMessage: "Tabela vazia",
            editRow: {
              saveTooltip: "Salvar",
              cancelTooltip: "Cancelar",
              deleteText: "Tem certeza que deseja excluir os dados?"
            }
          },
          toolbar: {
            searchPlaceholder: "Pesquisar",
            searchTooltip: "Pesquisar",
            exportTitle: "Exportar",
            exportAriaLabel: "Exportar",
            exportName: "Exportar CSV"
          },
          pagination: {
            labelRowsSelect: "linhas",
            labelRowsPerPage: "linhas por página",
            firstAriaLabel: "Primeira Página",
            firstTooltip: "Primeira Página",
            previousAriaLabel: "Página Anterior",
            previousTooltip: "Página Anterior",
            nextAriaLabel: "Próxima Página",
            nextTooltip: "Próxima Página",
            lastAriaLabel: "Última Página",
            lastTooltip: "Última Página"
          }
        }}
      />
    </div>
  );
}
