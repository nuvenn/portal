import React from "react";
import PropTypes from "prop-types";

export class GraphTooltip extends React.Component {
  static propTypes = {
    orientation: PropTypes.string,
    datum: PropTypes.object
  };

  render() {
    const { datum } = this.props;
    return (
      <g style={{ pointerEvents: "none" }}>
        <foreignObject x={0} y={5} width="100%" height="50">
          <div className="chart__tooltip">
            <span>{`${datum.status}`}</span>
          </div>
        </foreignObject>
      </g>
    );
  }
}

export default GraphTooltip;
