import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    width: 120,
    margin: 0
  },
  formText: {
    color: theme.palette.text.secondary
  }
}));

export default function SimpleSelect(props) {
  const classes = useStyles();
  return (
    <form className={classes.root} autoComplete="off">
      <FormControl className={classes.formControl}>
        <Select
          className={classes.formText}
          value={props.vinculo || ""}
          onChange={props.handleChange}
          inputProps={{
            name: props.name,
            id: `${props.name}-simple`
          }}
        >
          <MenuItem value="" disabled>
            {props.title}
          </MenuItem>
          {props.options.map(opt => {
            return (
              <MenuItem
                className={classes.formText}
                value={opt[props.keyValue]}
                key={opt[props.keyValue]}
              >
                {opt[props.show]}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </form>
  );
}
