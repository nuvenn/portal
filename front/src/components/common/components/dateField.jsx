import "moment";
import React from "react";
import { DatePicker } from "@material-ui/pickers";

export default function MaterialUIPickers(props) {
  const [selectedDate, setSelectedDate] = React.useState(new Date());

  function handleDateChange(date) {
    setSelectedDate(date);
  }

  return (
    <DatePicker
      views={["year"]}
      variant="inline"
      margin="normal"
      label="Ano"
      value={selectedDate}
      onChange={handleDateChange}
    />
  );
}
