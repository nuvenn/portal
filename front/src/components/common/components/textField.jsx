import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}));

export default function TextFields(props) {
  const classes = useStyles();
  return (
    <TextField
      onChange={props.handleChange}
      spacing={2}
      id={props.id}
      label={props.name}
      className={classes.textField}
      value={props.value}
      disabled={props.disabled}
      margin="normal"
      variant="outlined"
      fullWidth
    >
      {props.children}
    </TextField>
  );
}
