import React from "react";
import { DatePicker } from "@material-ui/pickers";

export default function MaterialUIPickers(props) {
  return (
    <DatePicker
      views={["year"]}
      variant="inline"
      margin="normal"
      label="Ano"
      value={props.value}
      onChange={props.onChange}
    />
  );
}
