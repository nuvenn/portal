import React from "react";
import MaterialTable from "material-table";

export default function DetailPanelWithRowClick(props) {
  return (
    <>
      <MaterialTable
        title={props.title}
        columns={props.columns}
        data={props.data}
        actions={[
          {
            icon: "visibility",
            tooltip: "Detalhes",
            onClick: (event, rowData) => props.handleClickOpen(rowData)
          }
        ]}
        options={{
          actionsColumnIndex: -1,
          exportButton: true
        }}
        localization={{
          header: {
            actions: "Ações"
          },
          body: {
            emptyDataSourceMessage: "Tabela vazia"
          },
          toolbar: {
            searchPlaceholder: "Pesquisar",
            searchTooltip: "Pesquisar",
            exportTitle: "Exportar",
            exportAriaLabel: "Exportar",
            exportName: "Exportar CSV"
          },
          pagination: {
            labelRowsSelect: "linhas",
            labelRowsPerPage: "linhas por página",
            firstAriaLabel: "Primeira Página",
            firstTooltip: "Primeira Página",
            previousAriaLabel: "Página Anterior",
            previousTooltip: "Página Anterior",
            nextAriaLabel: "Próxima Página",
            nextTooltip: "Próxima Página",
            lastAriaLabel: "Última Página",
            lastTooltip: "Última Página"
          }
        }}
      />
    </>
  );
}
