const INITIAL_STATE = {
  lista: [],
  ativo: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "VINCULOS_FETCHED":
      return {
        ...state,
        lista: action.payload.data,
        ativo: action.payload.data ? action.payload.data[0] : {}
      };
    case "VINCULO_ATIVO_SET":
      return {
        ...state,
        ativo: action.payload
      };
    default:
      return state;
  }
};
