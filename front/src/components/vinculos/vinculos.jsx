import React from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import Select from "../common/components/select";
import { setVinculoAtivo } from "../vinculos/vinculosActions";

export default function MediaControlCard(props) {
  const vinculos = useSelector(state => state.vinculos);
  const dispatch = useDispatch();

  function handleChange(event) {
    vinculos.lista.map(vinculo => {
      if (vinculo.numvinc === event.target.value) {
        dispatch(setVinculoAtivo(vinculo));
      }
      return vinculo;
    });
  }

  return (
    <Card className="card">
      <div className="card__details">
        <CardContent className="card__content">
          <Typography component="h6" variant="h6" className="card__title">
            Vínculo
          </Typography>
          <Grid container>
            <Grid item xs={12}>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                className="card__paragraph"
              >
                <Select
                  title="Vínculo"
                  name="blkvinculo"
                  keyValue="numvinc"
                  show="ident_vinc"
                  vinculo={props.vinculos.ativo.numvinc}
                  options={props.vinculos.lista}
                  handleChange={e => handleChange(e)}
                />
              </Typography>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                className="card__paragraph"
              >
                Cargo/Função: {props.vinculos.ativo.cargo_descr}
              </Typography>

              <Typography
                variant="subtitle2"
                color="textSecondary"
                className="card__paragraph"
              >
                Início do exercício:{" "}
                {moment(props.vinculos.ativo.dtexerc).format("DD-MM-YYYY")}
              </Typography>
              <Typography
                variant="subtitle2"
                color="textSecondary"
                className="card__paragraph"
              >
                Lotação:
              </Typography>

              <Typography
                variant="subtitle2"
                color="textSecondary"
                className="card__paragraph"
              >
                Situação: {props.vinculos.ativo.situacao}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </div>
      <CardMedia
        className="card__cover"
        alt="Foto do funcionário"
        title="Foto de identificação"
      >
        <img src={props.vinculos.ativo.fotoString} alt="Foto do funcionário" />
      </CardMedia>
    </Card>
  );
}
