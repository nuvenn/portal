// import { toastr } from "react-redux-toastr";
import HttpService from "../../services/HttpService";
import consts from "../../consts";

export function getVinculos(funcionario) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/Ergon/BlocoVinculo/${funcionario.numfunc}`
  );
  return {
    type: "VINCULOS_FETCHED",
    payload: request
  };
}

export function setVinculoAtivo(vinculo) {
  return {
    type: "VINCULO_ATIVO_SET",
    payload: vinculo
  };
}
