import HttpService from "../../services/HttpService";
import consts from "../../consts";

export function getHistorico(vinculo, ano) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/Vinculos/listVinculosPorNumfuncENumvincEPeriodo/${vinculo.numfunc}/${vinculo.numvinc}/''/''/${ano}`
  );
  return {
    type: "HISTORICO_VINCULOS_FETCHED",
    payload: request
  };
}

export function setHistoricoAtivo(vinculo) {
  return {
    type: "HISTORICO_VINCULOS_ATIVO_SET",
    payload: vinculo
  };
}
