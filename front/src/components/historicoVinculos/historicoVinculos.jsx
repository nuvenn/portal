import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useTheme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import PageHeader from "../common/template/pageHeader";
import BlocoVinculo from "../vinculos/vinculos";
import BoxContainer from "../common/components/box";
import TableDetails from "../common/components/tableDetails";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import { formatDate } from "../../services/DateService";
import { getVinculos } from "../vinculos/vinculosActions";
import { getHistorico, setHistoricoAtivo } from "./historicoVinculosActions";

export default function HistoricoVinculos() {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const auth = useSelector(state => state.auth);
  const vinculos = useSelector(state => state.vinculos);
  const historico = useSelector(state => state.historicoVinculos);
  const dispatch = useDispatch();

  const [selectedPeriod, setSelectedPeriod] = useState("TODOS");
  const [open, setOpen] = React.useState(false);

  const handlePeriodChange = period => {
    setSelectedPeriod(period);
  };

  const handleClickOpen = rowData => {
    dispatch(setHistoricoAtivo(rowData));
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (auth.logged === true) {
      dispatch(getVinculos(auth.user));
    }
  }, [dispatch, auth]);

  useEffect(() => {
    if (vinculos.ativo && vinculos.ativo.numfunc) {
      dispatch(getHistorico(vinculos.ativo, selectedPeriod));
    }
  }, [dispatch, vinculos.ativo, selectedPeriod]);

  return (
    <>
      <PageHeader title="Histórico de Vínculos" />
      <BoxContainer justify="center" marginTop={2}>
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <BlocoVinculo vinculos={vinculos} />
        </Grid>
      </BoxContainer>
      <BoxContainer justify="center">
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <ButtonGroup fullWidth aria-label="Escolha um período">
            <Button
              onClick={() => handlePeriodChange("ULTIMOS_2_ANOS")}
              variant="contained"
              color="primary"
            >
              Últimos 2 anos
            </Button>
            <Button
              onClick={() => handlePeriodChange("ULTIMOS_5_ANOS")}
              variant="contained"
              color="primary"
            >
              Últimos 5 anos
            </Button>
            <Button
              onClick={() => handlePeriodChange("ULTIMOS_10_ANOS")}
              variant="contained"
              color="primary"
            >
              Últimos 10 anos
            </Button>
            <Button
              onClick={() => handlePeriodChange("TODOS")}
              variant="contained"
              color="secondary"
            >
              Todos
            </Button>
          </ButtonGroup>
        </Grid>
      </BoxContainer>
      <BoxContainer justify="center" marginBottom={2}>
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <TableDetails
            title="Histórico"
            data={historico.lista}
            handleClickOpen={handleClickOpen}
            columns={[
              { title: "Número do vínculo", field: "numvinc" },
              {
                title: "Início do exercício",
                field: "dtexerc",
                render: (rowData = {}) =>
                  formatDate(rowData.dtexerc, "DD-MM-YYYY")
              },
              {
                title: "Encerramento",
                field: "dtEncerramento",
                render: (rowData = {}) =>
                  formatDate(rowData.dtEncerramento, "DD-MM-YYYY")
              },
              { title: "Detalhes do vínculo", field: "detalhesVinculo" }
            ]}
          />
        </Grid>
      </BoxContainer>
      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">DETALHES</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Cargo/Função: {historico.ativo.cargoDescr}
          </DialogContentText>
          <DialogContentText>
            Referência salarial: {historico.ativo.referencia}
          </DialogContentText>
          <DialogContentText>
            Lotação: {historico.ativo.setorDescr}
          </DialogContentText>
          <DialogContentText>
            Nomeação: {formatDate(historico.ativo.dtnom, "DD-MM-YYYY")}
          </DialogContentText>
          <DialogContentText>
            Posse: {formatDate(historico.ativo.dtposse, "DD-MM-YYYY")}
          </DialogContentText>
          <DialogContentText>
            Regime previdenciário: {historico.ativo.regimeprevDescr}
          </DialogContentText>
          <DialogContentText>
            Plano previdenciário: {historico.ativo.planoprevDescr}
          </DialogContentText>
          <DialogContentText>
            Empresa: {historico.ativo.empresaDescr}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="default" autoFocus>
            Fechar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
