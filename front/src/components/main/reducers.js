import { combineReducers } from "redux";
import { reducer as toastrReducer } from "react-redux-toastr";

import authReducer from "../auth/authReducer";
import vinculosReducer from "../vinculos/vinculosReducer";
import solicitacoesReducer from "../solicitacoes/solicitacoesReducer";
import situacaoFuncionalReducer from "../situacaoFuncional/situacaoFuncionalReducer";
import frequenciaReducer from "../frequencia/frequenciaReducer";
import historicoVinculosReducer from "../historicoVinculos/historicoVinculosReducer";

const rootReducer = combineReducers({
  toastr: toastrReducer,
  auth: authReducer,
  vinculos: vinculosReducer,
  solicitacoes: solicitacoesReducer,
  situacaoFuncional: situacaoFuncionalReducer,
  frequencia: frequenciaReducer,
  historicoVinculos: historicoVinculosReducer
});

export default rootReducer;
