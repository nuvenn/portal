import React from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import { Route } from "react-router-dom";
import { toastr } from "react-redux-toastr";

import Auth from "../auth/auth";
import Layout from "../main/layout";
import { refresh, logout } from "../auth/authActions";

export default function PrivateRoute(props) {
  const dispatch = useDispatch();

  axios.interceptors.response.use(
    response => {
      return response;
    },
    function(error) {
      if (error.response.status === 401) {
        toastr.error("Erro", "Operação não autorizada");
      } else if (error.response.status === 403) {
        dispatch(logout(logoutCallback));
        toastr.error("Erro", "A sessão está expirada");
      } else if (error.response.status === 404) {
        toastr.error("Erro", "Página não encontrada");
      } else if (error.response.status === 500) {
        toastr.error("Erro", "Erro de Servidor Interno");
      }
      return Promise.reject(error);
    }
  );

  function logoutCallback(response) {
    localStorage.clear();
    window.location.reload();
  }

  if (JSON.parse(localStorage.getItem("_u"))) {
    dispatch(refresh(JSON.parse(localStorage.getItem("_u"))));
    return (
      <>
        <Layout title="PORTAL DO FUNCIONÁRIO">
          <Route {...props} />
        </Layout>
      </>
    );
  } else {
    return <Auth />;
  }
}
