import React from "react";
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Auth from "../auth/auth";
import PrivateRoute from "./privateRoute";

import SituacaoFuncional from "../situacaoFuncional/situacaoFuncional";
import Solicitacoes from "../solicitacoes/solicitacoes";
import HistoricoVinculos from "../historicoVinculos/historicoVinculos";
import Frequencia from "../frequencia/frequencia";

export default () => (
  <Router>
    <Switch>
      <Route path="/" exact component={Auth}></Route>
      <PrivateRoute
        path="/situacaofuncional/situacaofuncional/"
        component={SituacaoFuncional}
      />
      <PrivateRoute
        path="/situacaofuncional/historicodevinculos/"
        component={HistoricoVinculos}
      />
      <PrivateRoute
        path="/solicitacoes/solicitacoes/"
        component={Solicitacoes}
      />
      <PrivateRoute
        path="/situacaofuncional/frequencia/"
        component={Frequencia}
      />
      <Redirect from="*" to="/" />
    </Switch>
  </Router>
);
