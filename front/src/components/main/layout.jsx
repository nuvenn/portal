import React from "react";
import Header from "../common/template/header";
import Footer from "../common/template/footer";
import Content from "../common/template/content";
import SideBar from "../common/template/sidebar";
import Main from "../common/template/main";

import "../../sass/App.scss";

export default props => (
  <div className="container">
    <Header />
    <Content>
      <SideBar />
      <Main>{props.children}</Main>
    </Content>
    <Footer title={props.title} />
  </div>
);
