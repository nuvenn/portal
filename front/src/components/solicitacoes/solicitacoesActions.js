import HttpService from "../../services/HttpService";
import { toastr } from "react-redux-toastr";
import consts from "../../consts";

export function getSolicitacoes(vinculo) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/Solicitacoes/Solicitacao/${vinculo.numfunc}/${vinculo.numvinc}/LinhaFuncional`
  );
  return {
    type: "SOLICITACOES_FETCHED",
    payload: request
  };
}

export function postSolicitacao(data) {
  return HttpService.request(
    "POST",
    `${consts.API_URL}/Solicitacoes/Solicitacao/${data.numfunc}/${data.numvinc}/LinhaFuncional/`,
    data
  )
    .then(() => {
      toastr.success("Sucesso", "Operação realizada com sucesso");
      return getSolicitacoes(data);
    })
    .catch(e => {
      toastr.error("Erro", "Não foi possível realizar a operação");
    });
}

export function updateSolicitacao(data) {
  return HttpService.request(
    "PUT",
    `${consts.API_URL}/Solicitacoes/Solicitacao/${data.numfunc}/${data.numvinc}/LinhaFuncional/${data.chave}`,
    data
  )
    .then(() => {
      toastr.success("Sucesso", "Operação realizada com sucesso");
      return getSolicitacoes(data);
    })
    .catch(e => {
      toastr.error("Erro", "Não foi possível realizar a operação");
    });
}

export function deleteSolicitacao(data) {
  return HttpService.request(
    "DELETE",
    `${consts.API_URL}/Solicitacoes/Solicitacao/9481281/1/LinhaFuncional/${data.chave}`
  )
    .then(() => {
      toastr.success("Sucesso", "Operação realizada com sucesso");
      return getSolicitacoes(data);
    })
    .catch(e => {
      toastr.error("Erro", "Não foi possível realizar a operação");
    });
}
