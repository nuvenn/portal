import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PageHeader from "../common/template/pageHeader";
import Table from "../common/components/table";
import BlocoVinculo from "../vinculos/vinculos";
import BoxContainer from "../common/components/box";

import { formatDate } from "../../services/DateService";
import { getVinculos } from "../vinculos/vinculosActions";
import {
  getSolicitacoes,
  postSolicitacao,
  updateSolicitacao,
  deleteSolicitacao
} from "./solicitacoesActions";

export default function Solicitacoes() {
  const auth = useSelector(state => state.auth);
  const vinculos = useSelector(state => state.vinculos);
  const solicitacoes = useSelector(state => state.solicitacoes);
  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.logged === true) {
      dispatch(getVinculos(auth.user));
    }
  }, [dispatch, auth]);

  useEffect(() => {
    if (vinculos.ativo && vinculos.ativo.numfunc) {
      dispatch(getSolicitacoes(vinculos.ativo));
    }
  }, [dispatch, vinculos.ativo]);

  function handleDelete(rowData) {
    dispatch(deleteSolicitacao(rowData));
  }

  function handleSubmit(type, rowData) {
    rowData.numfunc = vinculos.ativo.numfunc;
    rowData.numvinc = vinculos.ativo.numvinc;
    if (type === "add") {
      dispatch(postSolicitacao(rowData));
    } else if (type === "update") {
      dispatch(updateSolicitacao(rowData));
    }
  }

  return (
    <>
      <PageHeader title="Solicitações" />
      <BoxContainer justify="center" marginTop={2}>
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <BlocoVinculo vinculos={vinculos} />
        </Grid>
      </BoxContainer>
      <BoxContainer justify="center" marginBottom={2}>
        <Grid item xs={12} md={10} lg={10} xl={8}>
          <Table
            title="Solicitações"
            columns={[
              {
                title: "Cadastro",
                field: "data",
                render: (rowData = {}) =>
                  formatDate(rowData.data, "DD-MM-YYYY"),
                editable: "never"
              },
              { title: "Assunto", field: "assunto" },
              { title: "Solicitação", field: "solicitacao" },
              { title: "Status", field: "status", editable: "never" }
            ]}
            data={solicitacoes.lista}
            onAdd={handleSubmit}
            onEdit={handleSubmit}
            onDelete={handleDelete}
          />
        </Grid>
      </BoxContainer>
    </>
  );
}
