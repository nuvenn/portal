import HttpService from "../../services/HttpService";
import consts from "../../consts";

export function getFrequencia(vinculo, ano) {
  const request = HttpService.request(
    "GET",
    `${consts.API_URL}/SituacaoFuncional/Frequencias/listaEventosDeFrequenciasDoAno/${vinculo.numfunc}/${vinculo.numvinc}/${ano}`
  );
  return {
    type: "FREQUENCIA_FETCHED",
    payload: request
  };
}
