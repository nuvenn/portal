import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import FullCalendar from "@fullcalendar/react";
import brLocale from "@fullcalendar/core/locales/pt-br";
import interactionPlugin from "@fullcalendar/interaction";
import dayGridPlugin from "@fullcalendar/daygrid";
import Grid from "@material-ui/core/Grid";

import If from "../common/operator/if";
import PageHeader from "../common/template/pageHeader";
import BoxContainer from "../common/components/box";
import BlocoVinculo from "../vinculos/vinculos";
import YearField from "../common/components/yearField";
import { getVinculos } from "../vinculos/vinculosActions";
import { getFrequencia } from "./frequenciaActions";
import theme from "../../theme";

export default function Frequencia() {
  const auth = useSelector(state => state.auth);
  const vinculos = useSelector(state => state.vinculos);
  const frequencia = useSelector(state => state.frequencia);
  const dispatch = useDispatch();

  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleDateChange = date => {
    setSelectedDate(new Date(date));
  };

  useEffect(() => {
    if (auth.logged === true) {
      dispatch(getVinculos(auth.user));
    }
  }, [dispatch, auth]);

  useEffect(() => {
    if (vinculos.ativo && vinculos.ativo.numfunc) {
      dispatch(getFrequencia(vinculos.ativo, selectedDate.getFullYear()));
    }
  }, [dispatch, vinculos.ativo, selectedDate]);

  return (
    <>
      <PageHeader title="FREQUÊNCIA" />
      <BoxContainer justify="center" marginTop={2}>
        <Grid item xs={12} md={9}>
          <BlocoVinculo vinculos={vinculos} />
        </Grid>
      </BoxContainer>
      <BoxContainer justify="center" marginTop={2}>
        <YearField
          value={selectedDate}
          onChange={date => handleDateChange(date)}
          format="YYYY"
        />
      </BoxContainer>
      <BoxContainer justify="center" marginTop={2} marginBottom={2}>
        <Grid xs={12} md={9} spacing={2} item container>
          {frequencia.lista.map((mes, index) => {
            return (
              <If
                key={index}
                test={
                  new Date(mes.dtini).getFullYear() ===
                  selectedDate.getFullYear()
                }
              >
                <Grid item xs={12} sm={6} md={4}>
                  <FullCalendar
                    timeZone="UTC"
                    defaultView="dayGridMonth"
                    selectable={true}
                    fixedWeekCount={false}
                    locale={brLocale}
                    plugins={[dayGridPlugin, interactionPlugin]}
                    header={{
                      left: "title",
                      center: "",
                      right: ""
                    }}
                    defaultDate={mes.dtini}
                    events={frequencia.listaEventos}
                    eventClick={info => console.log(info)}
                    eventColor={theme.palette.primary.main}
                    showNonCurrentDates={false}
                  />
                </Grid>
              </If>
            );
          })}
        </Grid>
      </BoxContainer>
    </>
  );
}
