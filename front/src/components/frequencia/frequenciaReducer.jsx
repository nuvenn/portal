const INITIAL_STATE = {
  lista: [],
  listaEventos: []
};

function concatEventos(frequencia) {
  let eventos = [];
  frequencia.map(function(mes) {
    if (mes.frequencias) {
      mes.frequencias.map(function(evento) {
        eventos.push({
          id: evento.codfreq,
          title: evento.codfreqDescr,
          start: evento.dtini,
          end: evento.dtfim
        });
        return eventos;
      });
    }
    return eventos;
  });
  return eventos;
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "FREQUENCIA_FETCHED":
      return {
        ...state,
        lista: action.payload.data,
        listaEventos: concatEventos(action.payload.data)
      };
    default:
      return state;
  }
};
