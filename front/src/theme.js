import { createMuiTheme } from "@material-ui/core/styles";

//Padrão
const theme = createMuiTheme({
  typography: {
    fontSize: 22
  },
  palette: {
    primary: {
      main: "#0091ce",
      light: "#00a6ec",
      dark: "#00628b",
      greyLight1: "#f7f7f7",
      greyLight2: "#eee",
      greyDark: "#1f232c",
      greyDark2: "#434c63",
      colorWhite: "#fff",
      colorBlack: "#000"
    },
    secondary: {
      main: "#434c63"
    }
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "1.4rem"
      }
    }
  }
});

//Custom
// const theme = createMuiTheme({
//   typography: {
//     fontSize: 22
//   },
//   palette: {
//     primary: {
//       main: "#42b883",
//       light: "#00a6ec",
//       dark: "#35495e",
//       greyLight1: "#f7f7f7",
//       greyLight2: "#eee",
//       greyDark: "#1f232c",
//       greyDark2: "#434c63",
//       colorWhite: "#fff",
//       colorBlack: "#000"
//     },
//     secondary: {
//       main: "#434c63"
//     }
//   }
// });

export default theme;
