import React from "react";
import ReduxToastr from "react-redux-toastr";
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import promise from "redux-promise";
import multi from "redux-multi";
import thunk from "redux-thunk";
import moment from "moment";
import MomentUtils from "@date-io/moment";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import theme from "./theme";

import reducers from "./components/main/reducers";

const store = applyMiddleware(multi, thunk, promise)(createStore)(reducers);

export default props => {
  return (
    <MuiPickersUtilsProvider utils={MomentUtils} moment={moment}>
      <Provider store={store}>
        <ReduxToastr
          timeOut={3500}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          getState={state => state.toastr}
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar
          closeOnToastrClick
        />
        <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>
      </Provider>
    </MuiPickersUtilsProvider>
  );
};
