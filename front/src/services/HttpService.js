import axios from "axios";

export default {
  request: async function(method, url, data, headers) {
    let instance = {
      method: method,
      url: url,
      data: data,
      headers: headers ? headers : { "Content-type": "application/json" },
      withCredentials: true
    };
    try {
      return await axios(instance);
    } catch (error) {
      console.log(error);
    }
  }
};
