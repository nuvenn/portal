import moment from "moment";

export function formatDate(date, format) {
  return date ? moment(date).format(format) : "";
}
