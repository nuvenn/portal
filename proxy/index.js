var http = require("http");
const express = require("express");
const httpProxy = require("express-http-proxy");
const app = express();
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const helmet = require("helmet");

const portalProxy = httpProxy("http://192.168.0.120:8080");
const port = 5000;

// Proxy request
app.all("/*", (req, res, next) => {
  portalProxy(req, res, next);
});

app.use(logger("dev"));
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

var server = http.createServer(app);
server.listen(port, () => {
  console.log(`Server running at port: ${port}`);
});
